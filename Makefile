sourceFile := bolivia_million_alexa.csv
resultFile := bolivia_million_alexa.json
dir := domains
entelResolverIPv4 := 200.87.100.10
quad9ResolverIPv4 := 9.9.9.9
prefixBaseUrl := https://stat.ripe.net/data/prefix-overview/data.json?resource=%s
maxmindBaseUrl := https://stat.ripe.net/data/maxmind-geo-lite/data.json?resource=%s
ipmapBaseUrl := https://ipmap.ripe.net/api/v1/locate/%s/best
pitIPv4NetworkRegExp := ^179.0.30.

domainsList := $(shell cut -d, -f1 $(sourceFile))
domains := $(patsubst %,$(dir)/%,$(domainsList))
alexaRank := $(addsuffix /alexaRank,$(domains))
dnsARecord := $(addsuffix /dnsARecord,$(domains))
wwwDnsARecord := $(addsuffix /wwwDnsARecord,$(domains))
ipv4 := $(addsuffix /ipv4,$(domains))
prefix := $(addsuffix /prefix,$(domains))
ptr := $(addsuffix /ptr,$(domains))
maxmind := $(addsuffix /maxmind,$(domains))
ipmap := $(addsuffix /ipmap,$(domains))
traceroute := $(addsuffix /traceroute,$(domains))
pitipv4 := $(addsuffix /pitipv4,$(domains))
json := $(addsuffix /json,$(domains))

all: $(domains) $(alexaRank) $(dnsARecord) $(wwwDnsARecord) $(ipv4) $(prefix) $(ptr) $(maxmind) $(ipmap) $(traceroute) $(pitipv4) $(json) $(resultFile)

$(dir)/%: $(sourceFile) | $(dir)
	mkdir -p $@

$(dir)/%/alexaRank: | $(dir)/%
	grep '^$*,' $(sourceFile) | sed 's/.*,//' > $@

$(dir)/%/dnsARecord: | $(dir)/%
	dig @${entelResolverIPv4} +short $* A | grep -Eo '[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}' > $@

$(dir)/%/wwwDnsARecord: | $(dir)/%
	dig @${entelResolverIPv4} +short www.$* A | grep -Eo '[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}' > $@

$(dir)/%/ipv4: | $(dir)/%/dnsARecord $(dir)/%/wwwDnsARecord
	cat $(dir)/$*/dnsARecord $(dir)/$*/wwwDnsARecord | head -n 1 > $@

$(dir)/%/prefix: $(dir)/%/ipv4
	cat $^ | xargs -l printf ${prefixBaseUrl} | xargs -l curl -s | jq '.data // {}' > $@

$(dir)/%/ptr: $(dir)/%/ipv4
	cat $^ | xargs -l dig @${entelResolverIPv4} +short -x | jq --raw-input --slurp 'split("\n") | map(select(. != ""))' > $@

$(dir)/%/maxmind: $(dir)/%/ipv4
	cat $^ | xargs -l printf ${maxmindBaseUrl} | xargs -l curl -s | jq '.data.located_resources[0].locations[0] // {}' > $@

$(dir)/%/ipmap: $(dir)/%/ipv4
	cat $^ | xargs -l printf ${ipmapBaseUrl} | xargs -l curl -s | jq '.location // {}' > $@

$(dir)/%/traceroute: $(dir)/%/ipv4
	cat $^ | xargs -l mtr -o "B" -nz -c 1 --json | jq '.report.hubs // []' > $@

$(dir)/%/pitipv4: $(dir)/%/traceroute
	cat $^ | jq '[.[].host|select(. |match( "$(pitIPv4NetworkRegExp)"))]' > $@

$(dir)/%/json: $(dir)/%/alexaRank $(dir)/%/ipv4 $(dir)/%/prefix $(dir)/%/ptr $(dir)/%/maxmind $(dir)/%/ipmap $(dir)/%/traceroute $(dir)/%/pitipv4
	jq -n --arg domain $* \
		--arg alexaRank $$(cat $(dir)/$*/alexaRank) \
		--arg ipv4 $$(cat $(dir)/$*/ipv4) \
		--argfile prefix $(dir)/$*/prefix \
		--argfile ptr $(dir)/$*/ptr \
		--argfile maxmind $(dir)/$*/maxmind \
		--argfile ipmap $(dir)/$*/ipmap \
		--argfile traceroute $(dir)/$*/traceroute \
		--argfile pitipv4 $(dir)/$*/pitipv4 \
		'{domain: $$domain, alexaRank, $$alexaRank, ipv4: $$ipv4, prefix: $$prefix, ptr: $$ptr, maxmind: $$maxmind, ipmap: $$ipmap, traceroute: $$traceroute, pitipv4: $$pitipv4}' > $@

$(resultFile): $(json)
	jq -s . $^ > $@

$(dir):
	mkdir -p $(dir)

.PHONY: clean
clean:
	rm -rf $(dir) $(resultFile)
